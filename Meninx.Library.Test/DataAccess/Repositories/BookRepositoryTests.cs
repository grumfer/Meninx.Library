using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Bogus;
using Meninx.Library.DataAccess.Context;
using Meninx.Library.DataAccess.Repositories;
using Meninx.Library.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Meninx.Library.Test.DataAccess.Repositories
{
    [TestClass]
    public class BookRepositoryTests
    {
        private Mock<LibraryDBContext> _dbContextMock;
        private BookRepository _bookRepository;
        private Faker<Book> _fakerBook;

        [TestInitialize]
        public void TestInitialize()
        {
            _dbContextMock = new Mock<LibraryDBContext>();
            _bookRepository = new BookRepository(_dbContextMock.Object);
            _fakerBook = new Faker<Book>()
                .RuleFor(b => b.Id, f => f.Random.Long(1))
                .RuleFor(b => b.Title, f => f.Lorem.Sentence())
                .RuleFor(b => b.Author, f => f.Name.FullName());
        }

        [TestMethod]
        public void TestCreateAsync()
        {
            // Arrange
            var book = _fakerBook.Generate();
            _dbContextMock.Setup(db => 
                db.Set<Book>().Add(It.IsAny<Book>())).Returns(book);

            // Act
            _bookRepository.Add(book);

            // Assert
            _dbContextMock.Verify(db => db.Set<Book>().Add(book), Times.Once);
        }

        [TestMethod]
        public void TestGetById()
        {
            // Arrange
            var book = _fakerBook.Generate();
            _dbContextMock.Setup(db =>
                db.Set<Book>().Find(It.IsAny<long>())).Returns(book);

            // Act
            var result = _bookRepository.GetById(1);

            // Assert
            Assert.IsInstanceOfType(result, typeof(Book));
        }

        [TestMethod]
        public void TestGetById_ReturnsNull()
        {
            // Arrange
            var id = 1;
            _dbContextMock.Setup(db => db.Set<Book>().Find(It.IsAny<long>())).Returns((Book)null);

            // Act
            var result = _bookRepository.GetById(id);

            // Assert
            Assert.IsNull(result);
        }
    }
}
