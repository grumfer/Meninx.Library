using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Bogus;
using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Mapping.Contracts;
using Meninx.Library.BusinessLogic.Paging;
using Meninx.Library.BusinessLogic.Services;
using Meninx.Library.DataAccess.Repositories.Contracts;
using Meninx.Library.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Meninx.Library.Test.BusinessLogic.Services
{
    [TestClass]
    public class BookServiceTests
    {
        private Mock<IBookRepository> _bookRepositoryMock;
        private Mock<IMapper> _mapperMock;
        private BookService _bookService;
        private Faker<Book> _fakerBook;
        private Faker<BookDTO> _fakerBookDTO;
        private Faker<CreateBookDTO> _fakerCreateBookDTO;

        [TestInitialize]
        public void TestInitialize()
        {
            _bookRepositoryMock = new Mock<IBookRepository>();
            _mapperMock = new Mock<IMapper>();
            _bookService = new BookService(_bookRepositoryMock.Object, _mapperMock.Object);
            _fakerBook = new Faker<Book>()
                .RuleFor(b => b.Id, f => f.Random.Long(1))
                .RuleFor(b => b.Title, f => f.Lorem.Sentence())
                .RuleFor(b => b.Author, f => f.Name.FullName());
            _fakerBookDTO = new Faker<BookDTO>()
                .RuleFor(b => b.Id, f => f.Random.Long(1))
                .RuleFor(b => b.Title, f => f.Lorem.Sentence())
                .RuleFor(b => b.Author, f => f.Name.FullName());
            _fakerCreateBookDTO = new Faker<CreateBookDTO>()
                .RuleFor(b => b.Title, f => f.Lorem.Sentence())
                .RuleFor(b => b.Author, f => f.Name.FullName());
        }

        [TestMethod]
        public void TestGetGrid()
        {
            // Arrange
            var paginationFilter = new PaginationFilter();
            var fakeBooks = _fakerBook.Generate(10);
            var fakeBookDTOs = _fakerBookDTO.Generate(10);
            var expectedResult = new PagedResult<BookDTO>
            {
                Data = fakeBookDTOs,
                Meta = new MetaData(0, 10)
            };
            _bookRepositoryMock.Setup(br => 
                br.ExecuteGetStoredProcedure("GET_BOOKS", It.IsAny<SqlParameter[]>())).Returns(fakeBooks);
            _mapperMock.Setup(m => m.BooksToPagedResult(paginationFilter, fakeBooks)).Returns(expectedResult);

            // Act
            var result = _bookService.GetGrid(paginationFilter);

            // Assert
            Assert.IsInstanceOfType(result, typeof(PagedResult<BookDTO>));
        }

        [TestMethod]
        public void TestGetGrid_ThrowsException()
        {
            // Arrange
            var paginationFilter = new PaginationFilter();
            _bookRepositoryMock.Setup(br => 
                br.ExecuteGetStoredProcedure("GET_BOOKS", It.IsAny<SqlParameter[]>())).Throws(new Exception("Test exception"));

            // Act and Assert
            Assert.ThrowsException<Exception>(() => _bookService.GetGrid(paginationFilter));
        }

        [TestMethod]
        public async Task TestCreateAsync()
        {
            // Arrange
            var book = _fakerCreateBookDTO.Generate();
            var expectedBookDTO = new BookDTO
            {
                Id = 1,
                Title = book.Title,
                Author = book.Author,
                ISBN = book.ISBN,
                Quantity = book.Quantity,
                CategoryId = book.CategoryId
            };
            
            _mapperMock.Setup(m => m.BookToBookDTO(It.IsAny<Book>())).Returns(expectedBookDTO);

            // Act
            var result = await _bookService.CreateAsync(book, CancellationToken.None);

            // Assert
            Assert.IsInstanceOfType(result, typeof(BookDTO));
            Assert.AreEqual(expectedBookDTO, result);
        }

        [TestMethod]
        public async Task TestCreateAsync_ThrowsException()
        {
            // Arrange
            _bookRepositoryMock.Setup(br =>
                br.Add(It.IsAny<Book>())).Throws(new Exception("Test exception"));

            // Act and Assert
            await Assert.ThrowsExceptionAsync<Exception>(() => _bookService.CreateAsync(It.IsAny<CreateBookDTO>(), CancellationToken.None));
        }
    }
}
