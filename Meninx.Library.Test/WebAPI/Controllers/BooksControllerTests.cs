using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Bogus;
using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Paging;
using Meninx.Library.BusinessLogic.Services.Contracts;
using Meninx.Library.Domain.Models;
using Meninx.Library.WebAPI.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Meninx.Library.Test.WebAPI.Controllers
{
    [TestClass]
    public class BooksControllerTests
    {
        private Mock<IBookService> _bookServiceMock;
        private BooksController _bookController;
        private Faker<BookDTO> _faker;

        [TestInitialize]
        public void TestInitialize()
        {
            _bookServiceMock = new Mock<IBookService>();
            _bookController = new BooksController(_bookServiceMock.Object);
            _faker = new Faker<BookDTO>()
                .RuleFor(b => b.Id, f => f.Random.Long(1))
                .RuleFor(b => b.Title, f => f.Lorem.Sentence())
                .RuleFor(b => b.Author, f => f.Name.FullName());
        }

        [TestMethod]
        public void TestGetGrid()
        {
            // Arrange
            var paginationFilter = new PaginationFilter();
            var fakeBooks = _faker.Generate(10);
            var expectedResult = new PagedResult<BookDTO>
            {
                Data = fakeBooks,
                Meta = new MetaData(0, 10)
            };
            _bookServiceMock.Setup(bs => 
                bs.GetGrid(paginationFilter)).Returns(expectedResult);

            // Act
            var result = _bookController.GetGrid(paginationFilter);

            // Assert
            Assert.IsInstanceOfType(
                result, 
                typeof(OkNegotiatedContentResult<PagedResult<BookDTO>>));
        }

        [TestMethod]
        public void TestGetGrid_ReturnsEmptyResult()
        {
            // Arrange
            var paginationFilter = new PaginationFilter();
            var expectedResult = new PagedResult<BookDTO>
            {
                Data = new List<BookDTO>(),
                Meta = new MetaData(0, 0)
            };
            _bookServiceMock.Setup(bs => bs.GetGrid(paginationFilter)).Returns(expectedResult);

            // Act
            var result = _bookController.GetGrid(paginationFilter);

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<PagedResult<BookDTO>>));
            var contentResult = result as OkNegotiatedContentResult<PagedResult<BookDTO>>;
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(0, contentResult.Content.Data.Count);
        }

        [TestMethod]
        public async Task TestPostAsync()
        {
            // Arrange
            var createBookDTO = _faker.Generate();
            var expectedBookDTO = new BookDTO
            {
                Id = 1,
                Title = createBookDTO.Title,
                Author = createBookDTO.Author,
                ISBN = createBookDTO.ISBN,
                Quantity = createBookDTO.Quantity,
                CategoryId = createBookDTO.CategoryId
            };
            
            _bookServiceMock.Setup(bs => 
                bs.CreateAsync(createBookDTO, CancellationToken.None)).ReturnsAsync(expectedBookDTO);

            // Act
            var result = await _bookController.Post(createBookDTO);

            // Assert
            Assert.IsInstanceOfType(result, typeof(CreatedNegotiatedContentResult<BookDTO>));
            var createdResult = result as CreatedNegotiatedContentResult<BookDTO>;
            Assert.IsNotNull(createdResult);
            Assert.AreEqual(expectedBookDTO, createdResult.Content);
        }
    }
}