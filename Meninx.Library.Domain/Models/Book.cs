﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Meninx.Library.Domain.Enums;

namespace Meninx.Library.Domain.Models
{
    public class Book
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ScaffoldColumn(false)]
        public long Id { get; set; }

        [Required, StringLength(128)]
        public string Title { get; set; }

        [Required, StringLength(64)]
        public string Author { get; set; }

        [Required, StringLength(64)]
        public string ISBN { get; set; }

        public Quantity Quantity { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
    }
}