﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meninx.Library.Domain.Models
{
    public class Category
    {
        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required, StringLength(64)]
        public string Name { get; set; }

        [StringLength(256)]
        public string Description { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}