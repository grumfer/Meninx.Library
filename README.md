<h1 align="center"> Library </h1> <br>

<p align="center">
  Library WebForms and WebAPI is primarily responsible for view and change books.
</p>


## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Quick Start](#quick-start)
- [DataBase](#database)



## Features

* View books from UI and API.
* Edit books from UI and API.
* Add books from UI and API.
* Delete books from UI and API.


## Requirements
The application can be run locally using tools provided below.

* [.NET Framework 4.8]
* [Visual Studio](https://visualstudio.microsoft.com/downloads/) / [Visual Studio Code](https://code.visualstudio.com/Download) / [JetBrains Rider](https://www.jetbrains.com/rider/)


## Quick Start
You need run Update-Database command in Package Manager Console to create database.
To start WebAPI need to update startap url to "swagger" in WebApi project properties on Web tab.

### Visual Studio
![Visual Studio Launch Button](https://user-images.githubusercontent.com/1622880/64011288-031a0500-cae1-11e9-9238-b1a97569cf80.PNG)

### JetBrains Rider
![Rider Buttons](https://resources.jetbrains.com/help/img/rider/2022.1/run_debug_config_location.png)

Running the application via IIS on Windows uses the port `44349` and `44352`.

## DataBase
Application uses MS SQL Database. Connection strings to which can be found in Web.config in connectionStrings section.