﻿namespace Meninx.Library.DataAccess.Migrations
{
    using Domain.Enums;
    using Domain.Models;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Context.LibraryDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Context.LibraryDBContext dbContext)
        {
            GetCategories().ForEach(category => dbContext.Categories.Add(category));
            GetBooks().ForEach(book => dbContext.Books.Add(book));

            dbContext.SaveChanges();
        }

        private static List<Category> GetCategories()
        {
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Science Fiction",
                    Description = "Explores futuristic or imaginative concepts such as advanced science and technology, space exploration, time travel, and extraterrestrial life."
                },
                new Category
                {
                    Id = 2,
                    Name = "Horror",
                    Description = "Intended to evoke fear, disgust, or horror in the reader, often featuring supernatural elements, monsters, or psychological suspense."
                },
                new Category
                {
                    Id = 3,
                    Name = "Autobiography"
                }
            };

            return categories;
        }

        private static List<Book> GetBooks()
        {
            var books = new List<Book>
            {
                new Book
                {
                    Title = "It",
                    Author = "Stephen King",
                    ISBN = "9780340951453",
                    Quantity = (Quantity)2,
                    CategoryId = 2
                },
                new Book
                {
                    Title = "Dune",
                    Author = "Frank Herbert",
                    ISBN = "9780240807720",
                    Quantity = (Quantity)1,
                    CategoryId = 2
                },
                new Book
                {
                    Title = "The Autobiography of Malcolm X",
                    Author = "Alex Haley",
                    ISBN = "9780240807720",
                    Quantity = (Quantity)1,
                    CategoryId = 2
                }
            };

            return books;
        }
    }
}
