﻿namespace Meninx.Library.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 128),
                        Author = c.String(nullable: false, maxLength: 64),
                        ISBN = c.String(nullable: false, maxLength: 64),
                        Quantity = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);

            Sql(@"CREATE OR ALTER PROCEDURE [dbo].[GET_BOOKS]
                    @SEARCH_TEXT AS VARCHAR(50)='',
                    @SORT_COLUMN_NAME AS VARCHAR(50)='',
                    @SORT_COLUMN_DIRECTION AS VARCHAR(50)='',
                    @START_INDEX AS INT=0,
                    @PAGE_SIZE AS INT=10
                    AS
                    BEGIN
                     DECLARE @QUERY AS VARCHAR(MAX)='',@ORDER_QUERY AS VARCHAR(MAX)='',@CONDITIONS AS VARCHAR(MAX)='',
                     @PAGINATION AS VARCHAR(MAX)=''

                     SET @QUERY='SELECT * FROM Books '

                     -- SEARCH OPERATION
                     IF(ISNULL(@SEARCH_TEXT,'')<>'')
                     BEGIN
                      BEGIN
                       SET @CONDITIONS='
                       WHERE
                       Title LIKE ''%'+@SEARCH_TEXT+'%''
                       OR Author LIKE ''%'+@SEARCH_TEXT+'%''
                       OR ISBN LIKE ''%'+@SEARCH_TEXT+'%''
                      '
                      END
                     END

                     -- SORT OPERATION
                     IF(ISNULL(@SORT_COLUMN_NAME,'')<>'' AND ISNULL(@SORT_COLUMN_DIRECTION,'')<>'')
                     BEGIN
                      SET @ORDER_QUERY=' ORDER BY '+@SORT_COLUMN_NAME+' '+@SORT_COLUMN_DIRECTION
                     END
                     ELSE SET @ORDER_QUERY=' ORDER BY ID ASC'

                     -- PAGINATION OPERATION
                     IF(@PAGE_SIZE>0)
                     BEGIN
                      SET @PAGINATION=' OFFSET '+(CAST(@START_INDEX AS varchar(10)))+' ROWS
                      FETCH NEXT '+(CAST(@PAGE_SIZE AS varchar(10)))+' ROWS ONLY'
                     END

                     IF(@CONDITIONS<>'') SET @QUERY+=@CONDITIONS
                     IF(@ORDER_QUERY<>'') SET @QUERY+=@ORDER_QUERY
                     IF(@PAGINATION<>'') SET @QUERY+=@PAGINATION

                     EXEC(@QUERY)
                    END");
        }
        
        public override void Down()
        {
            Sql("DROP PROCEDURE [dbo].[GET_BOOKS]");
            DropForeignKey("dbo.Books", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Books", new[] { "CategoryId" });
            DropTable("dbo.Categories");
            DropTable("dbo.Books");
        }
    }
}
