﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Meninx.Library.DataAccess.Context;
using Meninx.Library.DataAccess.Repositories.Contracts;

namespace Meninx.Library.DataAccess.Repositories
{
    public class BaseRepository<TEntity, TKey> : IBaseRepository<TEntity, TKey> where TEntity : class
    {
        private readonly LibraryDBContext _dbContext;

        protected DbSet<TEntity> DbSet => _dbContext.Set<TEntity>();

        private bool _disposed;

        protected BaseRepository(LibraryDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual TEntity GetById(TKey id) =>
            DbSet.Find(id);

        public virtual Task<TEntity> GetByIdAsync(
            TKey id,
            CancellationToken cancellationToken = default)
        {
            return DbSet.FindAsync(cancellationToken, id);
        }

        public virtual IQueryable<TEntity> GetAll() =>
            DbSet.AsQueryable();

        public virtual async Task<IReadOnlyList<TEntity>> GetAllAsync(
            CancellationToken cancellationToken = default) =>
            await DbSet.ToListAsync(cancellationToken);

        public void Add(TEntity entity) =>
            DbSet.Add(entity);

        public void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Remove(TEntity entity) =>
            DbSet.Remove(entity);

        public int SaveChanges() =>
            _dbContext.SaveChanges();

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) =>
            _dbContext.SaveChangesAsync(cancellationToken);

        public IEnumerable<TEntity> ExecuteGetStoredProcedure(string procedureName, params SqlParameter[] parameters)
        {
            return _dbContext.Database.SqlQuery<TEntity>(
                $"{procedureName} {string.Join(", ", parameters.Select(p => p.ParameterName))}",
                parameters);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}