﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Meninx.Library.DataAccess.Repositories.Contracts
{
    public interface IBaseRepository<TEntity, in TKey> : IDisposable where TEntity : class
    {
        TEntity GetById(TKey id);

        Task<TEntity> GetByIdAsync(TKey id, CancellationToken cancellationToken = default);

        IQueryable<TEntity> GetAll();

        Task<IReadOnlyList<TEntity>> GetAllAsync(CancellationToken cancellationToken = default);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Remove(TEntity entity);

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        IEnumerable<TEntity> ExecuteGetStoredProcedure(string procedureName, params SqlParameter[] parameters);
    }
}