﻿using Meninx.Library.Domain.Models;

namespace Meninx.Library.DataAccess.Repositories.Contracts
{
    public interface IBookRepository : IBaseRepository<Book, long>
    {
    }
}