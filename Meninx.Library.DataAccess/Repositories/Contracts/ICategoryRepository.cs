﻿using Meninx.Library.Domain.Models;

namespace Meninx.Library.DataAccess.Repositories.Contracts
{
    public interface ICategoryRepository : IBaseRepository<Category, int>
    {
    }
}