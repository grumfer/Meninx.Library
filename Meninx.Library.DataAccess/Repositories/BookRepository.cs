﻿using Meninx.Library.DataAccess.Context;
using Meninx.Library.DataAccess.Repositories.Contracts;
using Meninx.Library.Domain.Models;

namespace Meninx.Library.DataAccess.Repositories
{
    public class BookRepository : BaseRepository<Book, long>, IBookRepository
    {
        public BookRepository(LibraryDBContext dbContext) : base(dbContext)
        {
        }
    }
}