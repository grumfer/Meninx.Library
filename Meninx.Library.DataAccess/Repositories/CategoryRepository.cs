﻿using Meninx.Library.DataAccess.Context;
using Meninx.Library.DataAccess.Repositories.Contracts;
using Meninx.Library.Domain.Models;

namespace Meninx.Library.DataAccess.Repositories
{
    public class CategoryRepository : BaseRepository<Category, int>, ICategoryRepository
    {
        public CategoryRepository(LibraryDBContext dbContext) : base(dbContext)
        {
        }
    }
}