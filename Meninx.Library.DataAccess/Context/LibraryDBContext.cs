﻿using System.Data.Entity;
using Meninx.Library.Domain.Models;

namespace Meninx.Library.DataAccess.Context
{
    public class LibraryDBContext : DbContext
    {
        public LibraryDBContext() : base("LibraryDBConnectionString")
        {
            var a = Database.Connection.ConnectionString;
        }
        
        public DbSet<Category> Categories { get; set; }
        
        public DbSet<Book> Books { get; set; }

    }
}