﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Mapping.Contracts;
using Meninx.Library.BusinessLogic.Paging;
using Meninx.Library.BusinessLogic.Services.Contracts;
using Meninx.Library.DataAccess.Repositories.Contracts;
using Meninx.Library.Domain.Models;

namespace Meninx.Library.BusinessLogic.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        
        private readonly IMapper _mapper;

        public BookService(IBookRepository bookRepository, IMapper mapper = null)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public ICollection<GridBookDTO> GetGrid()
        {
            return _bookRepository.GetAll().Select(book => new GridBookDTO
            {
                Id = book.Id,
                Author = book.Author,
                Title = book.Title,
                ISBN = book.ISBN,
                Quantity = book.Quantity.ToString(),
                CategoryName = book.Category.Name
            }).ToList();
        }

        public PagedResult<BookDTO> GetGrid(
            PaginationFilter paginationFilter)
        {
            var parameters = _mapper.PaginationFilterToSqlParameters(paginationFilter);
            
            var books = _bookRepository.ExecuteGetStoredProcedure("GET_BOOKS", parameters).ToList();

            return _mapper.BooksToPagedResult(paginationFilter, books);
        }

        public async Task<BookDTO> GetByIdAsync(
            long id, 
            CancellationToken cancellationToken = default)
        {
            var book = await _bookRepository.GetByIdAsync(id, cancellationToken);

            return _mapper.BookToBookDTO(book);
        }

        public BookDTO GetById(long id)
        {
            var book = _bookRepository.GetById(id);

            return _mapper.BookToBookDTO(book);
        }

        public void Create(CreateBookDTO bookDTO)
        {
            var book = _mapper.CreateBookDTOToBook(bookDTO);

            _bookRepository.Add(book);
            _bookRepository.SaveChanges();
        }

        public async Task<BookDTO> CreateAsync(
            CreateBookDTO bookDTO,
            CancellationToken cancellationToken = default)
        {
            var book = _mapper.CreateBookDTOToBook(bookDTO);

            _bookRepository.Add(book);
            await _bookRepository.SaveChangesAsync(cancellationToken);

            return _mapper.BookToBookDTO(book);
        }

        public void Update(BookDTO bookDTO)
        {
            var book = _bookRepository.GetById(bookDTO.Id);

            UpdateBookProperties(bookDTO, book);

            _bookRepository.Update(book);
            _bookRepository.SaveChanges();
        }

        public async Task<BookDTO> UpdateAsync(
            BookDTO bookDTO,
            CancellationToken cancellationToken = default)
        {
            var book = await _bookRepository.GetByIdAsync(bookDTO.Id, cancellationToken);

            UpdateBookProperties(bookDTO, book);

            _bookRepository.Update(book);
            await _bookRepository.SaveChangesAsync(cancellationToken);

            return _mapper.BookToBookDTO(book);
        }

        public void DeleteById(long id)
        {
            var book = _bookRepository.GetById(id);

            _bookRepository.Remove(book);
            _bookRepository.SaveChanges();
        }

        public async Task DeleteByIdAsync(
            long id,
            CancellationToken cancellationToken = default)
        {
            var book = await _bookRepository.GetByIdAsync(id, cancellationToken);
            
            _bookRepository.Remove(book);
            await _bookRepository.SaveChangesAsync(cancellationToken);
        }

        private void UpdateBookProperties(BookDTO bookDTO, Book book)
        {
            book.Author = bookDTO.Author;
            book.Title = bookDTO.Title;
            book.ISBN = bookDTO.ISBN;
            book.Quantity = bookDTO.Quantity;
            book.CategoryId = bookDTO.CategoryId;
        }
    }
}
