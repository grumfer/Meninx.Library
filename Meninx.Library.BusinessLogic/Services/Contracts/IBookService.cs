﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Paging;

namespace Meninx.Library.BusinessLogic.Services.Contracts
{
    public interface IBookService
    {
        ICollection<GridBookDTO> GetGrid();

        PagedResult<BookDTO> GetGrid(PaginationFilter paginationFilter);

        Task<BookDTO> GetByIdAsync(
            long id, 
            CancellationToken cancellationToken = default);
        
        BookDTO GetById(long id);

        void Create(CreateBookDTO bookDTO);

        Task<BookDTO> CreateAsync(
            CreateBookDTO bookDTO,
            CancellationToken cancellationToken = default);

        void Update(BookDTO bookDTO);
        
        Task<BookDTO> UpdateAsync(
            BookDTO bookDTO,
            CancellationToken cancellationToken = default);

        void DeleteById(long id);

        Task DeleteByIdAsync(
            long id,
            CancellationToken cancellationToken = default);
    }
}
