﻿namespace Meninx.Library.BusinessLogic.DTOs
{
    public class GridBookDTO
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string ISBN { get; set; }

        public string Quantity { get; set; }

        public string CategoryName { get; set; }
    }
}
