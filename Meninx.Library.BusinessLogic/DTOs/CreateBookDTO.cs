﻿using Meninx.Library.Domain.Enums;

namespace Meninx.Library.BusinessLogic.DTOs
{
    public class CreateBookDTO
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public string ISBN { get; set; }

        public Quantity Quantity { get; set; }

        public int CategoryId { get; set; }
    }
}
