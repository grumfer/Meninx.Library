﻿using Meninx.Library.Domain.Enums;

namespace Meninx.Library.BusinessLogic.DTOs
{
    public class BookDTO : CreateBookDTO
    {
        public long Id { get; set; }

        public BookDTO()
        {
        }

        public BookDTO(long id, string title, string author, string isbn, Quantity quantity, int categoryId)
        {
            Id = id;
            Title = title;
            Author = author;
            ISBN = isbn;
            Quantity = quantity;
            CategoryId = categoryId;
        }

        public void CopyFrom(CreateBookDTO createBookDTO)
        {
            Title = createBookDTO.Title;
            Author = createBookDTO.Author;
            ISBN = createBookDTO.ISBN;
            Quantity = createBookDTO.Quantity;
            CategoryId = createBookDTO.CategoryId;
        }
    }
}
