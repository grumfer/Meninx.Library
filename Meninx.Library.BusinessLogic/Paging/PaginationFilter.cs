﻿namespace Meninx.Library.BusinessLogic.Paging
{
    public class PaginationFilter
    {
        public string SearchText { get; set; }

        public int StartIndex { get; set; }

        public int PageSize { get; set; }

        public string SortColumnName { get; set; }

        public bool SortColumnDesc { get; set; }


        public PaginationFilter()
        {
            SearchText = string.Empty;
            StartIndex = 0;
            PageSize = 10;
            SortColumnName = "Id";
            SortColumnDesc = true;
        }

        public PaginationFilter(string searchText, int startIndex, int pageSize, string sortColumnName, bool sortColumnDesc)
        {
            SearchText = searchText;
            StartIndex = startIndex;
            PageSize = pageSize;
            SortColumnName = sortColumnName;
            SortColumnDesc = sortColumnDesc;
        }
    }
}
