﻿namespace Meninx.Library.BusinessLogic.Paging
{
    public class MetaData
    {
        public int StartIndex { get; set; }

        public int RowCount { get; set; }

        public MetaData()
        {
            StartIndex = default;
            RowCount = default;
        }

        public MetaData(int startIndex, int rowCount)
        {
            StartIndex = startIndex;
            RowCount = rowCount;
        }
    }
}
