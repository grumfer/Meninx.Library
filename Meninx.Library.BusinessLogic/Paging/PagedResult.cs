﻿using System.Collections.Generic;

namespace Meninx.Library.BusinessLogic.Paging
{
    public class PagedResult<T> where T : class
    {
        public ICollection<T> Data { get; set; }

        public MetaData Meta { get; set; }

        public PagedResult()
        {
            Data = new List<T>();
            Meta = new MetaData();
        }

        public PagedResult(MetaData meta, ICollection<T> data)
        {
            Meta = meta;
            Data = data;
        }
    }
}
