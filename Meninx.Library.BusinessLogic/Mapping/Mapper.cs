﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Mapping.Contracts;
using Meninx.Library.BusinessLogic.Paging;
using Meninx.Library.Domain.Models;

namespace Meninx.Library.BusinessLogic.Mapping
{
    public class Mapper : IMapper
    {
        public SqlParameter[] PaginationFilterToSqlParameters(PaginationFilter filter)
        {
            return new []
            {
                new SqlParameter("@SEARCH_TEXT", SqlDbType.VarChar) { Value = filter.SearchText },
                new SqlParameter("@SORT_COLUMN_NAME", SqlDbType.VarChar) { Value = filter.SortColumnName },
                new SqlParameter("@SORT_COLUMN_DIRECTION", SqlDbType.VarChar)
                {
                    Value = filter.SortColumnDesc ? "DESC" : "ASC"
                },
                new SqlParameter("@START_INDEX", SqlDbType.Int) { Value = filter.StartIndex },
                new SqlParameter("@PAGE_SIZE", SqlDbType.Int) { Value = filter.PageSize }
            };
        }

        public PagedResult<BookDTO> BooksToPagedResult(PaginationFilter paginationFilter, List<Book> books)
        {
            var bookDTOs = new List<BookDTO>(books.Select(BookToBookDTO));
           
            return new PagedResult<BookDTO>(
                new MetaData(paginationFilter.StartIndex, books.Count),
                bookDTOs);
        }

        public BookDTO BookToBookDTO(Book book)
        {
            return new BookDTO(book.Id, book.Title, book.Author, book.ISBN, book.Quantity, book.CategoryId);
        }

        public Book CreateBookDTOToBook(CreateBookDTO book)
        {
            return new Book
            {
                Title = book.Title,
                Author = book.Author,
                ISBN = book.ISBN,
                Quantity = book.Quantity,
                CategoryId = book.CategoryId,
            };
        }
    }
}
