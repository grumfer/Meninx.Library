﻿using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Paging;
using Meninx.Library.Domain.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Meninx.Library.BusinessLogic.Mapping.Contracts
{
    public interface IMapper
    {
        SqlParameter[] PaginationFilterToSqlParameters(PaginationFilter filter);
        
        PagedResult<BookDTO> BooksToPagedResult(PaginationFilter paginationFilter, List<Book> books);

        BookDTO BookToBookDTO(Book book);

        Book CreateBookDTOToBook(CreateBookDTO book);
    }
}
