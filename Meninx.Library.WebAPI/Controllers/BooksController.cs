﻿using System.Threading.Tasks;
using System.Web.Http;
using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Paging;
using Meninx.Library.BusinessLogic.Services.Contracts;

namespace Meninx.Library.WebAPI.Controllers
{
    public class BooksController : ApiController
    {
        private readonly IBookService _bookService;

        public BooksController(IBookService bookService)
        {
            _bookService = bookService;
        }

        public IHttpActionResult GetGrid([FromUri] PaginationFilter paginationFilter)
        {
            var result = _bookService.GetGrid(paginationFilter);
            
            return Ok(result);
        }

        public async Task<IHttpActionResult> Get([FromUri] long id)
        {
            var result = await _bookService.GetByIdAsync(id);

            return Ok(result);
        }

        public async Task<IHttpActionResult> Post([FromBody] CreateBookDTO createBookDTO)
        {
            var result = await _bookService.CreateAsync(createBookDTO);

            return Created(string.Empty, result);
        }

        public async Task<IHttpActionResult> Put([FromBody] BookDTO bookDTO)
        {
            var result = await _bookService.UpdateAsync(bookDTO);

            return Ok(result);
        }

        public async Task<IHttpActionResult> Delete([FromUri] long id)
        {
            await _bookService.DeleteByIdAsync(id);

            return Ok();
        }
    }
}
