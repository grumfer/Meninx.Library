using System.Web.Http;
using Meninx.Library.BusinessLogic.Mapping;
using Meninx.Library.BusinessLogic.Mapping.Contracts;
using Meninx.Library.BusinessLogic.Services;
using Meninx.Library.BusinessLogic.Services.Contracts;
using Meninx.Library.DataAccess.Context;
using Meninx.Library.DataAccess.Repositories;
using Meninx.Library.DataAccess.Repositories.Contracts;
using Unity;
using Unity.Injection;
using Unity.WebApi;

namespace Meninx.Library.WebAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IMapper, Mapper>();
            container.RegisterType<IBookService, BookService>();
            container.RegisterType<LibraryDBContext>(new InjectionConstructor());
            container.RegisterType<IBookRepository, BookRepository>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}