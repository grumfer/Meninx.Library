﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using Meninx.Library.WebForms.App_Start;

namespace Meninx.Library.WebForms
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityWebFormsStart.PostStart();
        }
    }
}