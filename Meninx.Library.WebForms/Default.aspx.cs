﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Meninx.Library.BusinessLogic.Services;
using Meninx.Library.BusinessLogic.Services.Contracts;
using Meninx.Library.DataAccess.Context;
using Meninx.Library.DataAccess.Repositories;

namespace Meninx.Library.WebForms
{
    public partial class Default : Page
    {
        private readonly IBookService _bookService = new BookService(new BookRepository(new LibraryDBContext()));

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGridView();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddOrEditBook.aspx");
        }

        protected void BooksGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var index = Convert.ToInt32(e.CommandArgument);
            var row = BooksGridView.Rows[index];

            var id = Convert.ToInt32(row.Cells[0].Text);

            switch (e.CommandName)
            {
                case "Edit_Book":
                    Response.Redirect($"AddOrEditBook.aspx?id={id}");
                    break;
                case "Delete_Book":
                    RemoveBook(id);
                    BindGridView();
                    break;
            }
        }

        private void RemoveBook(int id)
        {
            _bookService.DeleteById(id);
        }

        protected void BindGridView()
        {
            BooksGridView.DataSource = _bookService.GetGrid();
            BooksGridView.DataBind();
        }
    }
}