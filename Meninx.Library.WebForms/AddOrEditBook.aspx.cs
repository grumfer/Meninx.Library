﻿using System;
using System.Linq;
using System.Web.UI;
using Meninx.Library.BusinessLogic.DTOs;
using Meninx.Library.BusinessLogic.Mapping;
using Meninx.Library.BusinessLogic.Services;
using Meninx.Library.BusinessLogic.Services.Contracts;
using Meninx.Library.DataAccess.Context;
using Meninx.Library.DataAccess.Repositories;
using Meninx.Library.DataAccess.Repositories.Contracts;
using Meninx.Library.Domain.Enums;

namespace Meninx.Library.WebForms
{
    public partial class AddOrEditBook : Page
    {
        private readonly IBookService _bookService = new BookService(new BookRepository(new LibraryDBContext()), new Mapper());
        private readonly ICategoryRepository _categoryRepository = new CategoryRepository(new LibraryDBContext());

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateDropDownLists();

                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    var id = int.Parse(Request.QueryString["id"]);
                    PopulateEditForm(id);
                }
                else
                {
                    literalHeader.Text = "Add Book";
                    btnAddOrEditBook.Text = "Add Book";
                }
            }
        }

        private void PopulateEditForm(int id)
        {
            var book = _bookService.GetById(id);

            if (book != null)
            {
                literalHeader.Text = "Edit Book";
                btnAddOrEditBook.Text = "Edit Book";
                hiddenBookId.Value = id.ToString();
                textBoxTitle.Text = book.Title;
                textBoxAuthor.Text = book.Author;
                textBoxISBN.Text = book.ISBN;
                dropDownQuantity.SelectedValue = book.Quantity.ToString();
                dropDownCategory.SelectedValue = book.CategoryId.ToString();
            }
        }

        private void PopulateDropDownLists()
        {
            var quantityValues = Enum.GetValues(typeof(Quantity));
            dropDownQuantity.DataSource = quantityValues;
            dropDownQuantity.DataBind();

            var categories = _categoryRepository.GetAll().ToList();
            dropDownCategory.DataSource = categories;
            dropDownCategory.DataTextField = "Name";
            dropDownCategory.DataValueField = "Id";
            dropDownCategory.DataBind();
        }

        protected void btnAddOrEditBook_Click(object sender, EventArgs e)
        {
            Enum.TryParse(dropDownQuantity.SelectedValue, out Quantity quantity);

            var book = new CreateBookDTO
            {
                Title = textBoxTitle.Text,
                Author = textBoxAuthor.Text,
                ISBN = textBoxISBN.Text,
                Quantity = quantity,
                CategoryId = Convert.ToInt32(dropDownCategory.SelectedValue)
            };

            if (!string.IsNullOrEmpty(hiddenBookId.Value))
            {
                var bookId = int.Parse(hiddenBookId.Value);
                var bookToUpdate = new BookDTO();
                bookToUpdate.CopyFrom(book);
                bookToUpdate.Id = bookId;
                
                _bookService.Update(bookToUpdate);
            }
            else
            {
                _bookService.Create(book);
            }

            Response.Redirect("Default.aspx");
        }
    }
}