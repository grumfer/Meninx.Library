﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Meninx.Library.WebForms.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <main aria-labelledby="title">
        <h2 id="title"><%: Title %>.</h2>
        <h3>Your contact page.</h3>

        <address>
            <strong>Support:</strong>   <a href="mailto:nazarii.nazar96@gmail.com">nazarii.nazar96@gmail.com</a><br />
        </address>
    </main>
</asp:Content>