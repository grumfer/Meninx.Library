﻿<%@ Page Title="Library" Language="C#" EnableEventValidation="true" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Meninx.Library.WebForms.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <head>
        <script>
            function confirmDelete() {
                return confirm("Are you sure you want to delete this item?");
            }
        </script>
    </head>
    <asp:GridView ID="BooksGridView" runat="server" AutoGenerateColumns="False" OnRowCommand="BooksGridView_RowCommand">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" />
            <asp:BoundField DataField="Title" HeaderText="Title" />
            <asp:BoundField DataField="Author" HeaderText="Author" />
            <asp:BoundField DataField="ISBN" HeaderText="ISBN" />
            <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
            <asp:BoundField DataField="CategoryName" HeaderText="Quantity" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button runat="server" Text="Edit" CommandName="Edit_Book" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button runat="server" Text="Delete" CommandName="Delete_Book" CausesValidation="False" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" OnClientClick="return confirmDelete();"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Button ID="btnAdd" runat="server" Text="Add Book" OnClick="btnAdd_Click" />
</asp:Content>