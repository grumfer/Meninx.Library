﻿<%@ Page Title="Add Book" Language="C#" EnableEventValidation="true" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddOrEditBook.aspx.cs" Inherits="Meninx.Library.WebForms.AddOrEditBook" %>

<asp:Content ID="AddBookContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h2><asp:Literal ID="literalHeader" runat="server" /></h2>
        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <asp:TextBox ID="textBoxTitle" runat="server" placeholder="Title" CssClass="form-control" /><br />
        <asp:TextBox ID="textBoxAuthor" runat="server" placeholder="Author" CssClass="form-control" /><br />
        <asp:TextBox ID="textBoxISBN" runat="server" placeholder="ISBN" CssClass="form-control" /><br />
        <asp:DropDownList ID="dropDownQuantity" runat="server" placeholder="Quantity" style = "margin-bottom: 25px" CssClass="form-control"></asp:DropDownList>
        <asp:DropDownList ID="dropDownCategory" runat="server" placeholder="Category" style = "margin-bottom: 25px" CssClass="form-control"></asp:DropDownList>
        <asp:Button ID="btnAddOrEditBook" runat="server" Text="Add Book" OnClick="btnAddOrEditBook_Click" />
        <asp:HiddenField ID="hiddenBookId" runat="server" />
    </div>
</asp:Content>